# PWall - Periodic Wallpaper Utility
Periodically change the desktop wallpaper, sourcing images from online databases
such as Unsplash, danbooru (safebooru), and Konachan.net.

For Linux, macOS and Windows.

## Usage
To get a new wallpaper, click the "Next Wallpaper" button.

To limit your wallpaper to only images matching certain tags, fill in the "Tags"
textbox.

To edit your wallpaper, navigate to the "Effects" tab, change the sliders and
click "Apply".

Check out previous wallpapers by navigating to the "History" tab and clicking
the "Open Wallpaper Folder" button.

### Enabling the automatic background service
#### Linux
1. `systemctl enable pwalld`

#### macOS
To be completed.

#### Windows
1. Open File Explorer and navigate to the folder containing the program.
1. Copy `pwalld.exe`.
1. Enter `shell:startup` into the File Explorer path and press <kbd>Enter</kbd>.
1. Right-click the blank space and select `Paste shortcut`.

## Dependencies
- [Qt] >= 5.11
- [ImageMagick] >= 7.0 ([ImageMagick license])

## Building
First, make sure all the dependencies are met. Then:
```sh
mkdir build bin
pushd build
cmake ..
popd
cmake --build build --config Release
```
The executable will be built into the `pwall/bin` directory.

## Attributions
- Inspired by [Muzei by Roman Nurik]

[Qt]: https://www.qt.io
[ImageMagick]: https://www.imagemagick.org/script/index.php
[ImageMagick license]: https://www.imagemagick.org/script/license.php
[Muzei by Roman Nurik]: http://muzei.co

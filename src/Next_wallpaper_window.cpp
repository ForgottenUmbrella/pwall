#include "Next_wallpaper_window.h"
#include "Wallpaper_changer.h"
#include "Image_data.h"
#include <QWidget>
#include <QDialog>
#include <QProgressBar>

namespace pwall {
    Next_wallpaper_window::Next_wallpaper_window(QWidget* parent) :
        QDialog{parent}, changer{this}
    {
        ui.setupUi(this);
        connect(
            &changer, &Wallpaper_changer::data_progress,
            this, &Next_wallpaper_window::set_progress
        );
        connect(
            &changer, &Wallpaper_changer::data_received,
            ui.progress_bar, &QProgressBar::reset
        );
        connect(
            &changer, &Wallpaper_changer::data_received,
            this, &Next_wallpaper_window::set_downloading_message
        );
        connect(
            &changer, &Wallpaper_changer::download_progress,
            this, &Next_wallpaper_window::set_progress
        );
        connect(
            &changer, &Wallpaper_changer::downloaded,
            ui.progress_bar, &QProgressBar::reset
        );
        connect(
            &changer, &Wallpaper_changer::downloaded,
            this, &Next_wallpaper_window::set_editing_message
        );
        connect(
            &changer, &Wallpaper_changer::downloaded,
            this, &Next_wallpaper_window::set_unknown_progress
        );
        connect(
            &changer, &Wallpaper_changer::succeeded,
            this, &Next_wallpaper_window::accept
        );
        connect(
            &changer, &Wallpaper_changer::failed,
            this, &Next_wallpaper_window::reject
        );
        connect(
            this, &Next_wallpaper_window::rejected,
            &changer, &Wallpaper_changer::cancel
        );
    }

    void Next_wallpaper_window::set_progress(int received, int total)
    {
        const auto size_unknown = total == -1;
        if (size_unknown)
        {
            set_unknown_progress();
        } else {
            ui.progress_bar->setMaximum(total);
            ui.progress_bar->setValue(received);
        }
    }

    void Next_wallpaper_window::set_unknown_progress()
    {
        const auto unknown = 0;
        ui.progress_bar->setMaximum(unknown);
        ui.progress_bar->setValue(unknown);
    }

    void Next_wallpaper_window::set_downloading_message()
    {
        ui.wait_label->setText(
            "Please wait while the wallpaper is being downloaded..."
        );
    }

    void Next_wallpaper_window::set_editing_message()
    {
        ui.wait_label->setText(
            "Please wait while the wallpaper is being edited..."
        );
    }

    void Next_wallpaper_window::disable_cancel()
    {
        ui.dialog_buttons->setEnabled(false);
    }
};

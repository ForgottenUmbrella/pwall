#include "effects.h"
#include "Settings.h"
#include <Magick++.h>
#include <QString>
#include <QGuiApplication>
#include <QScreen>
#include <cstddef>

namespace pwall {
    void edit(
        const QString& source, const QString& destination,
        const Settings& settings
    )
    {
        auto image = Magick::Image{source.toStdString()};
        // Only blur if the blur preference is set, otherwise the image
        // will be unnecessarily scaled down and rescaled up, resulting
        // in data loss.
        if (const auto radius = blur_radius(settings)) {
            image = scaled_blur(image, radius);
        }
        const auto same_hue = 100.0;
        image.modulate(brightness(settings), saturation(settings), same_hue);
        // Ensure images are saved at full quality.
        // By default, ImageMagick saves images with 75% compression,
        // leading to lower-quality images.
        const auto no_compression = 100;
        image.quality(no_compression);
        image.write(destination.toStdString());
    }

    Magick::Image scaled_blur(Magick::Image image, double radius)
    {
        const auto original_size = image.size();
        const auto scale_ratio = 0.25;
        image = scale(image, scale_ratio);
        const auto scaled_blur_radius = radius / scale_ratio;
        image.blur(scaled_blur_radius);
        // Use `Image::resize` instead of `Image::scale` so that the
        // image scales smoothly without looking pixelated.
        image.resize(original_size);
        return image;
    }

    double blur_radius(const Settings& settings)
    {
        const auto relative_max = settings.blur / 10'000.0;
        const auto screen = QGuiApplication::primaryScreen()
            ->availableGeometry();
        const auto max_radius = relative_max * screen.height();
        return max_radius;
    }

    Magick::Image scale(Magick::Image image, double ratio)
    {
        const auto scaled_width = static_cast<std::size_t>(
            image.rows() * ratio
        );
        const auto scaled_height = static_cast<std::size_t>(
            image.columns() * ratio
        );
        const auto scaled_size = Magick::Geometry{scaled_width, scaled_height};
        image.scale(scaled_size);
        return image;
    }
};

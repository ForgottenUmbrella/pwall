#include "Downloader.h"
#include "Network_requester.h"
#include <QObject>
#include <QUrl>
#include <QNetworkReply>
#include <QString>
#include <QFile>
#include <QIODevice>
#include <memory>
#include <stdexcept>

namespace pwall {
    Downloader::Downloader(QObject* parent) :
        QObject{parent}, requester{this}
    {
        connect(
            &requester, &Network_requester::download_progress,
            this, &Downloader::progress
        );
        connect(
            &requester, &Network_requester::succeeded,
            this, [this](std::shared_ptr<QNetworkReply> reply) {
                try {
                    save(*reply, write_path);
                    emit succeeded(write_path);
                } catch (std::runtime_error& e) {
                    emit failed(e.what());
                }
            }
        );
        connect(
            &requester, &Network_requester::failed,
            this, &Downloader::failed
        );
        connect(
            &requester, &Network_requester::finished,
            this, &Downloader::finished
        );
    }

    void Downloader::start(const QUrl& url, const QString& path)
    {
        requester.get(url);
        write_path = path;
    }

    void Downloader::cancel()
    {
        requester.cancel();
    }

    void save(QIODevice& reply, const QString& path)
    {
        if (auto file = QFile{path}; file.open(QIODevice::WriteOnly)) {
            const auto data = reply.readAll();
            file.write(data);
        } else {
            throw std::runtime_error{"Can't open file for writing"};
        }
    }
};

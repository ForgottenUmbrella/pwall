#include "Network_requester.h"
#include <QObject>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>

namespace pwall {
    Network_requester::Network_requester(QObject* parent) :
        QObject{parent}, network_manager{this}
    {
        connect(
            &network_manager, &QNetworkAccessManager::finished,
            this, &Network_requester::finished
        );
        connect(
            &network_manager, &QNetworkAccessManager::finished,
            this, [this](QNetworkReply* reply) {
                emit success_or_error(*reply);
            }
        );
    }

    void Network_requester::get(const QUrl& url)
    {
        const auto request = QNetworkRequest{url};
        const auto reply = network_manager.get(request);
        reply_manager.reset(reply);
        connect(
            reply, &QNetworkReply::downloadProgress,
            this, &Network_requester::download_progress
        );
    }

    void Network_requester::cancel()
    {
        if (reply_manager) {
            reply_manager->abort();
        }
    }

    void Network_requester::success_or_error(QNetworkReply& reply)
    {
        if (const auto error_code = reply.error(); !error_code) {
            emit succeeded(reply_manager);
        } else if (error_code != QNetworkReply::OperationCanceledError) {
            emit failed(error_message(error_code));
        }
    }

    QString error_message(QNetworkReply::NetworkError error_code)
    {
        switch (error_code) {
        case QNetworkReply::HostNotFoundError:
            return "There is no internet connection.";
        case QNetworkReply::TimeoutError:
            return "The network request timed out.";
        case QNetworkReply::ContentAccessDenied:
            [[fallthrough]];
        case QNetworkReply::ServiceUnavailableError:
            [[fallthrough]];
        case QNetworkReply::BackgroundRequestNotAllowedError:
            return QString{
                "Platform policy does not allow this network request.\n"
                "(Error code: %1)"
            }.arg(QString::number(error_code));
        case QNetworkReply::AuthenticationRequiredError:
            [[fallthrough]];
        case QNetworkReply::InternalServerError:
            return QString{
                "There are too many tags, or some might be restricted. "
                "You need to provide a valid account for the source.\n"
                "(Error code: %1)"
            }.arg(QString::number(error_code));
        case QNetworkReply::ContentNotFoundError:
            return "There are no images that match the given tags.";
        case QNetworkReply::ProtocolUnknownError:
            return "There might be no images that match the given tags, "
                "or your proxy server is blocking the network request.\n"
                "(Unknown protocol error)";
        case QNetworkReply::OperationCanceledError:
            // Should never occur given the guard against it in
            // `success_or_error`.
            return "Operation was cancelled.";
        default:
            return QString{
                "Something went wrong.\n(Error code: %1)"
            }.arg(QString::number(error_code));
        }
    }
};

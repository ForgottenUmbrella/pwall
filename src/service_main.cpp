#include "init.h"
#include "Notifier.h"
#include "Settings.h"
#include "Wallpaper_changer.h"
#include "Promise.h"
#include <QObject>
#include <QDebug>
#include <chrono>
#include <thread>
#include <ctime>

// Run the background service that automatically changes the wallpaper.
int main(int argc, char* argv[])
{
    // GUI elements (notifications) depend on this variable's lifetime.
    const auto _ = pwall::init(argc, argv);
    auto notifier = pwall::Notifier{};
    notifier.show();
    auto wallpaper_changer = pwall::Wallpaper_changer{};
    QObject::connect(
        &wallpaper_changer, &pwall::Wallpaper_changer::failed,
        &notifier, &pwall::Notifier::error
    );
    while (true) {
        const auto settings = pwall::read_settings();
        const auto now = std::chrono::system_clock::now();
        const auto when = std::chrono::time_point<std::chrono::system_clock>{
            std::chrono::seconds{settings.scheduled_time}
        };
        const auto when_time_t = std::chrono::system_clock::to_time_t(when);
        qDebug() << "Changing wallpaper at" << std::ctime(&when_time_t);
        if (now >= when) {
            auto change_wallpaper = [&wallpaper_changer, &settings]{
                qDebug() << "Time to change the wallpaper!";
                wallpaper_changer.start(settings);
            };
            pwall::Promise{
                change_wallpaper,
                wallpaper_changer, &pwall::Wallpaper_changer::finished
            }.await();
        }
        qDebug() << "Waiting...";
        std::this_thread::sleep_for(
            std::chrono::hours{settings.rotation_interval}
        );
    }
}

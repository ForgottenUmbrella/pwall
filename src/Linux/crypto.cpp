#include "crypto.h"
#include "blowfish/blowfish.h"
#include <unistd.h>
#include <QString>
#include <QByteArray>
#include <vector>
#include <string>

namespace {
    // Convert a `QString` to a `std::vector<char>`.
    std::vector<char> to_vector(const QString& string)
    {
        const auto std_string = string.toStdString();
        return {std_string.begin(), std_string.end()};
    }

    // Convert a `QByteArray` to a `std::vector<char>`.
    std::vector<char> to_vector(const QByteArray& array)
    {
        return {array.begin(), array.end()};
    }

    // Convert a `std::vector<char>` to a `QByteArray`.
    QByteArray to_qbytearray(const std::vector<char>& vector)
    {
        return {vector.data(), static_cast<int>(vector.size())};
    }
}

namespace pwall {
    QByteArray user_encrypt(const QByteArray& plaintext)
    {
        const auto key = QString::number(getuid());
        return to_qbytearray(
            Blowfish{to_vector(key)}.Encrypt(to_vector(plaintext))
        );
    }

    QByteArray user_decrypt(const QByteArray& cyphertext)
    {
        const auto key = QString::number(getuid());
        return to_qbytearray(
            Blowfish{to_vector(key)}.Decrypt(to_vector(cyphertext))
        );
    }
};

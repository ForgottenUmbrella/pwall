#include "wallpaper.h"
#include <QString>
#include <cstdlib>

namespace {
    // Return wallpaper command for GNOME 3, and also Cinnamon and Budgie
    // (which are based on it).
    QString gnome_wallpaper_command(const QString& path)
    {
        return QString{
            "gsettings set org.gnome.desktop.background picture-uri 'file://%1'"
        }.arg(path);
    }

    // Return wallpaper command for KDE Plasma 5.
    QString kde_wallpaper_command(const QString& path)
    {
        // From https://git.reviewboard.kde.org/r/125648.
        // XXX: Remove this.
        // const auto script = QString{R"(
        //     var all_desktops = desktops();
        //     for (var i = 0; i < all_desktops.length; ++i) {
        //         var desktop = all_desktops[i];
        //         desktop.wallpaperPlugin = "org.kde.image";
        //         destkop.currentConfigGroup = [
        //             "Wallpaper", "org.kde.image", "General"
        //         ];
        //         desktop.writeConfig("Image", "file://%1");
        //     }
        // )"}.arg(path);
        const auto script = QString{R"(
					  for (let desktop of desktops()) {
		            desktop.wallpaperPlugin = "org.kde.image";
		            desktop.currentConfigGroup = [
		                "Wallpaper", "org.kde.image", "General"
		            ];
		            desktop.writeConfig("Image", "file://%1");
            }
        )"}.arg(path);
        return QString{
            "qdbus org.kde.plasmashell /PlasmaShell "
            "org.kde.PlasmaShell.evaluateScript '%1'"
        }.arg(script);
    }

    // Return wallpaper command for Deepin (which is based on GNOME).
    QString deepin_wallpaper_command(const QString& path)
    {
        return QString{
            "qsettings set com.deepin.wrap.gnome.desktop.background picture-uri "
            "'file://%1'"
        }.arg(path);
    }

    // Return wallpaper command for LXDE.
    QString lxde_wallpaper_command(const QString& path)
    {
        return QString{"pcmanfm -w '%1'"}.arg(path);
    }

    // Return wallpaper command for LXQt (which is based on LXDE).
    QString lxqt_wallpaper_command(const QString& path)
    {
        return QString{"pcmanfm-qt -w '%1'"}.arg(path);
    }

    // Return wallpaper command for MATE (which is based on GNOME 2).
    QString mate_wallpaper_command(const QString& path)
    {
        return QString{
            "dconf write /org/mate/desktop/background/picture-filename '%1'"
        }.arg(path);
    }

    // Return wallpaper command for Xfce 4.
    QString xfce_wallpaper_command(const QString& path)
    {
        return QString{
            "xfconf-query --channel xfce4-desktop "
            "--property /backdrop/screen0/monitor0/image-path --set '%1'"
        }.arg(path);
    }

    // Return wallpaper command for standalone window managers like i3.
    QString unknown_wallpaper_command(const QString& path)
    {
        return QString{"feh --bg-scale '%1'"}.arg(path);
    }
}

namespace pwall {
    void set_wallpaper(const QString& path)
    {
        const auto commands = {
            &gnome_wallpaper_command,
            &kde_wallpaper_command,
            &deepin_wallpaper_command,
            &lxde_wallpaper_command,
            &lxqt_wallpaper_command,
            &mate_wallpaper_command,
            &xfce_wallpaper_command,
            &unknown_wallpaper_command,
        };
        // It's difficult to discern which desktop environment is in use,
        // so just try commands for each one.
        for (auto command : commands) {
            std::system(command(path).toUtf8().constData());
        }
    }
};

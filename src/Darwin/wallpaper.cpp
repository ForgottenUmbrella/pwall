#include "wallpaper.h"
#include <QString>
#include <cstdlib>

namespace pwall {
    void set_wallpaper(const QString& path)
    {
        const auto script = QString{
            R"(tell application "Finder" to set desktop picture to POSIX file "%1")"
        }.arg(path);
        const auto command = QString{"osascript -e '%1'"}.arg(script);
        std::system(command.toUtf8().constData());
    }
};

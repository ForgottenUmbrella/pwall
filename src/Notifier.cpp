#include "Notifier.h"
#include <QObject>
#include <QIcon>
#include <QString>
#include <QSystemTrayIcon>
#include <QDebug>

namespace pwall {
    Notifier::Notifier(QObject* parent) :
        QObject{parent}, system_tray_icon{QIcon{":/icon"}, this}
    {
    }

    void Notifier::notify(
        const QString& title, const QString& message,
        QSystemTrayIcon::MessageIcon icon, int timeout
    )
    {
        system_tray_icon.showMessage(title, message, icon, timeout);
    }

    void Notifier::show()
    {
        system_tray_icon.show();
    }

    void Notifier::error(const QString& message)
    {
        notify("Error", message, QSystemTrayIcon::Warning);
    }
};

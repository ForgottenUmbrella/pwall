#include "init.h"
#include "Main_window.h"

// Run the program.
int main(int argc, char* argv[])
{
    auto application = pwall::init(argc, argv);
    auto main_window = pwall::Main_window{};
    main_window.show();
    return application->exec();
}

#include "Tag_history_window.h"
#include "ui_Tag_history_window.h"
#include "Settings.h"
#include <QWidget>
#include <QDialog>
#include <QPushButton>
#include <QtAlgorithms>

namespace pwall {
    Tag_history_window::Tag_history_window(QWidget* parent) : QDialog{parent}
    {
        ui.setupUi(this);
        const auto settings = read_settings();
        load_history(settings);
        connect(
            ui.remove_button, &QPushButton::clicked,
            this, [this] {
                qDeleteAll(ui.tag_history_list->selectedItems());
            }
        );
        connect(
            ui.remove_button, &QPushButton::clicked,
            this, &Tag_history_window::disable_remove_if_empty
        );
        connect(
            this, &Tag_history_window::accepted,
            this, &Tag_history_window::save_history
        );
    }

    void Tag_history_window::load_history(const Settings& settings)
    {
        ui.tag_history_list->addItems(settings.tag_history);
        disable_remove_if_empty();
    }

    void Tag_history_window::save_history() const
    {
        auto settings = read_settings();
        settings.tag_history.clear();
        const auto tags_list = ui.tag_history_list;
        const auto num_tags = tags_list->count();
        for (auto i = 0; i < num_tags; ++i) {
            const auto tag_set = tags_list->item(i)->text();
            settings.tag_history.append(tag_set);
        }
        settings.tag_history.removeDuplicates();
        settings.tag_history = trim_tag_history(settings);
        write(settings);
    }

    void Tag_history_window::disable_remove_if_empty()
    {
        const auto tag_list_empty = !ui.tag_history_list->count();
        if (tag_list_empty) {
            ui.remove_button->setEnabled(false);
        }
    }
};

#include "Unsplash_source.h"
#include "Image_provider.h"
#include "Network_requester.h"
#include "Settings.h"
#include "Image_data.h"
#include <QObject>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QUrlQuery>
#include <QDebug>
#include <memory>

namespace pwall {
    Unsplash_source::Unsplash_source(QObject* parent) :
        Image_provider{parent}, requester{this}
    {
        connect(
            &requester, &Network_requester::download_progress,
            this, &Unsplash_source::data_progress
        );
        connect(
            &requester, &Network_requester::succeeded,
            this, [this](std::shared_ptr<QNetworkReply> reply) {
                parse_data(*reply);
            }
        );
        connect(
            &requester, &Network_requester::failed,
            this, &Unsplash_source::failed
        );
        connect(
            &requester, &Network_requester::finished,
            this, &Unsplash_source::finished
        );
    }

    bool Unsplash_source::is_limited_by_account() const
    {
        return false;
    }

    void Unsplash_source::start_getting_data(const Settings& settings)
    {
        const auto post_url = unsplash_post(settings);
        requester.get(post_url);
        qDebug() << "Unsplash post URL:" << post_url;
    }

    QUrl unsplash_post(const Settings& settings)
    {
        auto post_url = QUrl{"https://api.unsplash.com/photos/random"};
        const auto query = unsplash_query(settings);
        post_url.setQuery(query);
        return post_url;
    }

    QUrlQuery unsplash_query(const Settings& settings)
    {
        auto query = QUrlQuery{};
        const auto access_key = "aab234a7bbe513e6c55e2ef0f8dd0eaf4595fd6c8cd3d131447d28e1cf030b16";
        query.addQueryItem("client_id", access_key);
        query.addQueryItem("query", settings.tags);
        query.addQueryItem("orientation", "landscape");
        return query;
    }

    void Unsplash_source::cancel()
    {
        requester.cancel();
    }

    void Unsplash_source::parse_data(QNetworkReply& reply)
    {
        const auto data = reply.readAll();
        const auto post = QJsonDocument::fromJson(data);
        const auto download_url = QUrl{post["urls"]["full"].toString()};
        const auto webpage_url = QUrl{post["links"]["html"].toString()};
        const auto image_data = Image_data{download_url, webpage_url};
        emit data_received(image_data);
    }
};

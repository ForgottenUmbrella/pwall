#include "init.h"
#include <QApplication>
#include <Magick++.h>

namespace pwall {
    std::unique_ptr<QApplication> init(int argc, char* argv[])
    {
        QApplication::setOrganizationName("ForgottenUmbrella");
        QApplication::setApplicationName("PWall");
        Magick::InitializeMagick(nullptr);
        auto app = std::make_unique<QApplication>(argc, argv);
        return app;
    }
};

#include "Account_window.h"
#include "ui_Account_window.h"
#include "Settings.h"
#include <QWidget>
#include <QDialog>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>

namespace pwall {
    Account_window::Account_window(QWidget* parent) : QDialog{parent}
    {
        ui.setupUi(this);
        const auto settings = read_settings();
        load_into_ui(settings);
        const auto any_input = ui.username_input->text() + ui.key_input->text();
        enable_remove_button(any_input);
        for (auto& input : {ui.username_input, ui.key_input}) {
            connect(
                input, &QLineEdit::textChanged,
                this, &Account_window::enable_remove_button
            );
            connect(
                ui.remove_button, &QPushButton::clicked,
                input, &QLineEdit::clear
            );
        }
        connect(
            ui.remove_button, &QPushButton::clicked,
            ui.remove_button, [this] {
                ui.remove_button->setEnabled(false);
            }
        );
        connect(
            this, &Account_window::accepted,
            this, &Account_window::save_credentials
        );
    }

    void Account_window::accept()
    {
        const auto username = ui.username_input->text();
        const auto key = ui.key_input->text();
        if (is_valid(username, key)) {
            QDialog::accept();
        } else {
            const auto reason = "Both username and API key must be filled in, "
                "or both must be empty.";
            QMessageBox::warning(
                this, "Invalid Account", reason, QMessageBox::Ok
            );
        }
    }

    void Account_window::load_into_ui(const Settings& settings)
    {
        ui.username_input->setText(settings.username);
        ui.key_input->setText(settings.key);
    }

    void Account_window::save_credentials() const
    {
        auto settings = read_settings();
        settings.username = ui.username_input->text();
        settings.key = ui.key_input->text().toUtf8();
        write(settings);
    }

    void Account_window::enable_remove_button(const QString& new_text)
    {
        const auto has_text = !new_text.isEmpty();
        ui.remove_button->setEnabled(has_text);
    }

    bool is_valid(const QString& username, const QString& key)
    {
        const auto username_is_empty = username.isEmpty();
        const auto key_is_empty = key.isEmpty();
        const auto has_no_credentials = username_is_empty && key_is_empty;
        const auto has_full_credentials = !username_is_empty && !key_is_empty;
        return has_no_credentials || has_full_credentials;
    }
};

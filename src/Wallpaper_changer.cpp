#include "Wallpaper_changer.h"
#include "Image_provider_factory.h"
#include "Image_provider.h"
#include "effects.h"
#include "wallpaper.h"
#include "Image_data.h"
#include "Settings.h"
#include "Downloader.h"
#include <QObject>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>
#include <QUrl>
#include <QFutureWatcher>
#include <QtConcurrent>
#include <QStringList>
#include <QDebug>
#include <chrono>
#include <algorithm>

namespace pwall {
    Wallpaper_changer::Wallpaper_changer(QObject* parent) :
        QObject{parent},
        provider{Image_provider_factory::get(read_settings().source, this)},
        image_downloader{this}
    {
        connect(
            provider.get(), &Image_provider::data_progress,
            this, &Wallpaper_changer::data_progress
        );
        connect(
            provider.get(), &Image_provider::data_received,
            this, &Wallpaper_changer::data_received
        );
        connect(
            provider.get(), &Image_provider::data_received,
            this, &Wallpaper_changer::start_downloading
        );
        connect(
            provider.get(), &Image_provider::data_received,
            this, &Wallpaper_changer::save_webpage_url
        );
        connect(
            &image_downloader, &Downloader::progress,
            this, &Wallpaper_changer::download_progress
        );
        connect(
            &image_downloader, &Downloader::succeeded,
            this, &Wallpaper_changer::downloaded
        );
        connect(
            &image_downloader, &Downloader::succeeded,
            this, &save_wallpaper_path
        );
        connect(
            &image_downloader, &Downloader::succeeded,
            [] {
                const auto settings = read_settings();
                trim_cache(settings);
            }
        );
        connect(
            &image_downloader, &Downloader::succeeded,
            this, [this] {
                const auto settings = read_settings();
                maybe_edit(settings);
            }
        );
        connect(
            &edits_watcher, &QFutureWatcher<QString>::finished,
            this, [this] {
                qDebug() << "Editing finished";
                const auto wallpaper_path = edits_watcher.result();
                emit wallpaper_ready(wallpaper_path);
            }
        );
        connect(
            this, &Wallpaper_changer::wallpaper_ready,
            this, [](const QString& wallpaper_path) {
                qDebug() << "Set wallpaper path:" << wallpaper_path;
                set_wallpaper(wallpaper_path);
            }
        );
        connect(&image_downloader, &Downloader::succeeded, &schedule);
        connect(
            this, &Wallpaper_changer::wallpaper_ready,
            this, &Wallpaper_changer::succeeded
        );
        connect(
            provider.get(), &Image_provider::failed,
            this, &Wallpaper_changer::failed
            );
        connect(
            &image_downloader, &Downloader::failed,
            this, &Wallpaper_changer::failed
        );
        connect(
            provider.get(), &Image_provider::finished,
            this, &Wallpaper_changer::finished
        );
        connect(
            &image_downloader, &Downloader::finished,
            this, &Wallpaper_changer::finished
        );
    }

    void Wallpaper_changer::start(const Settings& settings)
    {
        provider->start_getting_data(settings);
    }

    void Wallpaper_changer::cancel()
    {
        qDebug() << "Cancelling provider...";
        provider->cancel();
        qDebug() << "Cancelling downloader...";
        image_downloader.cancel();
        edits_watcher.waitForFinished();
        qDebug() << "Waiting for editing to finish...";
    }

    void Wallpaper_changer::start_downloading(const Image_data& data)
    {
        const auto dir = image_dir();
        const auto url = QUrl{data.download_url};
        const auto path = QString{"%1/%2"}.arg(dir.path(), url.fileName());
        mkdir_nested(dir);
        image_downloader.start(url, path);
        qDebug() << "Download URL:" << url;
        qDebug() << "Download path:" << path;
    }

    void Wallpaper_changer::save_webpage_url(const Image_data& data)
    {
        auto settings = read_settings();
        settings.webpage_url = data.webpage_url;
        qDebug() << "Webpage URL:" << data.webpage_url;
        write(settings);
        emit webpage_url_changed(data.webpage_url);
    }

    void Wallpaper_changer::maybe_edit(const Settings& settings)
    {
        if (has_effect_prefs(settings)) {
            const auto new_path = edited_path(settings.wallpaper_path);
            // Ensure the directory exists before writing to it.
            mkdir_nested(edits_dir());
            const auto editing_future = QtConcurrent::run(
                [settings, new_path] {
                    edit(settings.wallpaper_path, new_path, settings);
                    return new_path;
                }
            );
            edits_watcher.setFuture(editing_future);
            qDebug() << "Editing wallpaper...";
        } else {
            emit wallpaper_ready(settings.wallpaper_path);
        }
    }

    void save_wallpaper_path(const QString& path)
    {
        auto settings = read_settings();
        settings.wallpaper_path = path;
        write(settings);
    }

    void schedule()
    {
        auto settings = read_settings();
        const auto now = std::chrono::system_clock::now();
        const auto interval = std::chrono::hours{settings.rotation_interval};
        const auto new_time = (now + interval).time_since_epoch();
        using std::chrono::duration_cast;
        settings.scheduled_time = duration_cast<std::chrono::seconds>(
            new_time
        ).count();
        write(settings);
    }

    void trim_cache(const Settings& settings)
    {
        const auto any_filetype = QStringList{"*"};
        for (auto dir : {image_dir(), edits_dir()}) {
            // A list of files, sorted by time from oldest to newest.
            const auto files = dir.entryList(
                any_filetype, QDir::Files, QDir::Time | QDir::Reversed
            );
            const auto num_files = files.count();
            const auto num_extra = std::max(
                num_files - settings.wallpaper_history_limit, 0
            );
            for (auto i = 0; i < num_extra; ++i) {
                const auto oldest_file = files[i];
                dir.remove(oldest_file);
            }
        }
    }

    QDir image_dir()
    {
        return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    }

    QDir edits_dir()
    {
        return image_dir().path() + "/edits";
    }

    QString edited_path(const QString& path)
    {
        const auto file_info = QFileInfo{path};
        return QString{"%1/%2"}.arg(edits_dir().path(), file_info.fileName());
    }

    void mkdir_nested(const QDir& path)
    {
        QDir{}.mkpath(path.path());
    }
};

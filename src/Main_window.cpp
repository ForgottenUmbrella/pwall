#include "Main_window.h"
#include "ui_Main_window.h"
#include "Image_provider_factory.h"
#include "Image_provider.h"
#include "Settings.h"
#include "Account_window.h"
#include "Tag_history_window.h"
#include "Next_wallpaper_window.h"
#include "Image_data.h"
#include "Wallpaper_changer.h"
#include "wallpaper.h"
#include <QWidget>
#include <QMainWindow>
#include <QPixmap>
#include <QPushButton>
#include <QComboBox>
#include <QSlider>
#include <QSpinBox>
#include <QString>
#include <QUrl>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QtGlobal>
#include <QDesktopServices>
#include <QToolTip>
#include <QPoint>
#include <QDebug>
#include <memory>
#include <map>

namespace pwall {
    Main_window::Main_window(QWidget* parent) :
        QMainWindow{parent}, notifier{this}
    {
        ui.setupUi(this);
        notifier.show();
        populate_source_input(Image_provider_factory::providers);
        load_warning_image(QPixmap{":/info"});
        sync_sliders();
        const auto settings = read_settings();
        load_into_ui(settings);
        const auto source = ui.source_input->currentText();
        update_ui_for_source(source);
        enable_apply_button(false);
        enable_apply_button_on_change();
        auto disable_apply_button = [this] {
            enable_apply_button(false);
        };
        auto next_wallpaper_clicked = [this, disable_apply_button] {
            const auto settings = save_settings();
            next_wallpaper(settings);
            disable_apply_button();
        };
        connect(
            ui.next_wallpaper_button, &QPushButton::clicked,
            this, next_wallpaper_clicked
        );
        connect(
            ui.tags_input->lineEdit(), &QLineEdit::returnPressed,
            this, next_wallpaper_clicked
        );
        const auto& apply_button = get_apply_button();
        connect(
            &apply_button, &QPushButton::clicked,
            this, [this] {
                const auto old_settings = read_settings();
                const auto new_settings = save_settings();
                edit_if_changed(old_settings, new_settings);
            }
        );
        connect(
            &apply_button, &QPushButton::clicked,
            this, disable_apply_button
        );
        connect(
            ui.dialog_buttons, &QDialogButtonBox::rejected,
            this, &Main_window::close
        );
        connect(
            ui.source_input, &QComboBox::currentTextChanged,
            this, &Main_window::update_ui_for_source
        );
        connect(
            ui.account_button, &QPushButton::clicked,
            this, &open_modal<Account_window>
        );
        connect(
            ui.wallpaper_folder_button, &QPushButton::clicked,
            open_image_dir
        );
        connect(
            ui.tag_history_button, &QPushButton::clicked,
            this, &open_modal<Tag_history_window>
        );
    }

    QPushButton& Main_window::get_apply_button()
    {
        return *ui.dialog_buttons->button(QDialogButtonBox::Apply);
    }

    void Main_window::populate_source_input(
        const Image_provider_factory::Map& providers
    )
    {
        for (const auto& [provider_name, _] : providers) {
            ui.source_input->addItem(provider_name);
        }
    }

    void Main_window::sync_sliders()
    {
        const auto sliders_inputs = std::map<QSlider*, QSpinBox*>{
            {ui.blur_slider, ui.blur_input},
            {ui.dim_slider, ui.dim_input},
            {ui.grey_slider, ui.grey_input},
        };
        for (auto [slider, input] : sliders_inputs) {
            connect(slider, &QSlider::valueChanged, input, &QSpinBox::setValue);
            connect(
                input, qOverload<int>(&QSpinBox::valueChanged),
                slider, &QSlider::setValue
            );
        }
    }

    void Main_window::enable_apply_button_on_change()
    {
        // Default arguments don't work with `connect`, so provide a lambda
        // that takes zero arguments instead.
        auto enable_apply_button = [this] {
            this->enable_apply_button();
        };
        for (const auto combobox : {ui.source_input, ui.tags_input}) {
            connect(
                combobox, &QComboBox::currentTextChanged,
                this, enable_apply_button
            );
        }
        const auto spinboxes = {
            ui.blur_input, ui.dim_input, ui.grey_input,
            ui.rotation_interval_input, ui.wallpaper_limit_input
        };
        for (const auto spinbox : spinboxes) {
            connect(
                spinbox, qOverload<int>(&QSpinBox::valueChanged),
                this, enable_apply_button
            );
        }
    }

    void Main_window::load_warning_image(const QPixmap& warning_image)
    {
        const auto width = ui.source_warning->width();
        const auto height = ui.source_warning->height();
        const auto scaled_warning = warning_image.scaled(
            width, height, Qt::KeepAspectRatio
        );
        ui.source_warning->setPixmap(scaled_warning);
    }

    void Main_window::load_into_ui(const Settings& settings)
    {
        ui.source_input->setCurrentText(settings.source);
        ui.tags_input->addItems(settings.tag_history);
        ui.tags_input->setCurrentText(settings.tags);
        ui.blur_input->setValue(settings.blur);
        ui.dim_input->setValue(settings.dim);
        ui.grey_input->setValue(settings.grey);
        ui.rotation_interval_input->setValue(settings.rotation_interval);
        ui.wallpaper_limit_input->setValue(settings.wallpaper_history_limit);
        set_link_url(settings.webpage_url);
    }

    void Main_window::enable_apply_button(bool enabled)
    {
        auto& apply_button = get_apply_button();
        apply_button.setEnabled(enabled);
    }

    void Main_window::enable_sign_in(bool enabled)
    {
        ui.source_warning->setEnabled(enabled);
        if (enabled) {
            ui.source_warning->setToolTip(
                "If you aren't signed in, "
                "only a maximum of two tags may be used."
            );
        } else {
            ui.source_warning->setToolTip(
                "This source does not require signing in."
            );
        }
        ui.account_button->setEnabled(enabled);
    }

    Settings Main_window::save_settings() const
    {
        auto settings = read_settings();
        settings.source = ui.source_input->currentText();
        settings.tags = ui.tags_input->currentText();
        settings.wallpaper_history_limit = ui.wallpaper_limit_input->value();
        settings.blur = ui.blur_input->value();
        settings.dim = ui.dim_input->value();
        settings.grey = ui.grey_input->value();
        settings.rotation_interval = ui.rotation_interval_input->value();
        // Update tag history.
        if (!settings.tag_history.contains(settings.tags)) {
            ui.tags_input->addItem(settings.tags);
            settings.tag_history.append(settings.tags);
            settings.tag_history.removeDuplicates();
            settings.tag_history = trim_tag_history(settings);
        }
        write(settings);
        return settings;
    }

    void Main_window::set_link_url(const QUrl& url)
    {
        const auto description = "View wallpaper online";
        if (!url.isEmpty()) {
            const auto linked_text = QString{"<a href='%1'>%2</a>"}.arg(
                url.toString(), description
            );
            ui.view_online_link->setText(linked_text);
            ui.view_online_link->setEnabled(true);
        } else {
            ui.view_online_link->setText(description);
            ui.view_online_link->setEnabled(false);
        }
        qDebug() << "Set link URL:" << url;
    }

    void Main_window::update_ui_for_source(const QString& source)
    {
        const auto provider = Image_provider_factory::get(source, this);
        enable_sign_in(provider->is_limited_by_account());
        show_tooltip(*ui.source_warning);
    }

    void Main_window::next_wallpaper(const Settings& settings)
    {
        auto next_wallpaper_window = Next_wallpaper_window{this};
        connect(
            &next_wallpaper_window.changer,
            &Wallpaper_changer::webpage_url_changed,
            this, &Main_window::set_link_url
        );
        connect(
            &next_wallpaper_window.changer, &Wallpaper_changer::failed,
            &notifier, &Notifier::error
        );
        next_wallpaper_window.changer.start(settings);
        next_wallpaper_window.exec();
    }

    void Main_window::edit_wallpaper(const Settings& settings)
    {
        auto progress_window = Next_wallpaper_window{this};
        progress_window.changer.maybe_edit(settings);
        progress_window.disable_cancel();
        progress_window.set_editing_message();
        progress_window.set_unknown_progress();
        progress_window.exec();
    }

    void Main_window::edit_if_changed(
        const Settings& old_settings, const Settings& new_settings
    )
    {
        const auto has_prefs = has_effect_prefs(new_settings);
        // Whether the current wallpaper was set by this program.
        // This boolean prevents trying to edit a non-existent wallpaper
        // on first run, when there is no wallpaper yet.
        const auto has_pwall_wallpaper = !new_settings.wallpaper_path.isEmpty();
        const auto should_edit = has_prefs
            && different_effects(old_settings, new_settings)
            && has_pwall_wallpaper;
        if (should_edit) {
            edit_wallpaper(new_settings);
        } else if (!has_prefs) {
            // Set original, unedited wallpaper if the user has set
            // all of their effect preferences to zero.
            set_wallpaper(new_settings.wallpaper_path);
        }
    }

    void open_image_dir()
    {
        const auto dir = image_dir();
        const auto uri = QUrl::fromLocalFile(dir.path());
        mkdir_nested(dir);
        QDesktopServices::openUrl(uri);
        qDebug() << "Image dir:" << uri;
    }

    void show_tooltip(const QWidget& widget)
    {
        const auto widget_position = widget.mapToGlobal(QPoint{0, 0});
        QToolTip::showText(widget_position, widget.toolTip());
    }
};

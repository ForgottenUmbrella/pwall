#include "wallpaper.h"
#include <QString>
#include <QtGlobal>
#include <QDebug>
#include <windows.h>

namespace {
    // Set wallpaper using the Windows API.
    void set_wallpaper(const char path[])
    {
        const auto unneeded_param = 0;
        const auto success = SystemParametersInfo(
            SPI_SETDESKWALLPAPER, unneeded_param,
            // Discard constness, since Windows' API uses type-unsafe
            // void pointers...
            const_cast<char*>(path),
            SPIF_UPDATEINIFILE
        );
        qDebug() << "Set wallpaper success:" << success;
    }
};

namespace pwall {
    void set_wallpaper(const QString& path)
    {
        ::set_wallpaper(qUtf8Printable(path));
    }
};

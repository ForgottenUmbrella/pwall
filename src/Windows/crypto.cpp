#include "crypto.h"
#include <QByteArray>
#include <windows.h>
#include <dpapi.h>

namespace pwall {
    QByteArray user_encrypt(const QByteArray& plaintext)
    {
        auto unencrypted_data = DATA_BLOB{
            static_cast<DWORD>(plaintext.size()),
            reinterpret_cast<BYTE*>(const_cast<char*>(plaintext.data()))
        };

        const auto no_description = nullptr;
        const auto no_entropy = nullptr;
        const auto reserved_param = nullptr;
        const auto no_prompt = nullptr;
        const auto no_flags = 0;
        auto encrypted_data = DATA_BLOB{};
        CryptProtectData(
            &unencrypted_data, no_description, no_entropy, reserved_param,
            no_prompt, no_flags, &encrypted_data
        );

        const auto encrypted_bytes = encrypted_data.pbData;
        const auto encrypted_length = encrypted_data.cbData;
        const auto cyphertext = QByteArray{
            reinterpret_cast<const char*>(encrypted_bytes),
            static_cast<int>(encrypted_length)
        };
        return cyphertext;
    }

    QByteArray user_decrypt(const QByteArray& cyphertext)
    {
        auto encrypted_data = DATA_BLOB{
            static_cast<DWORD>(cyphertext.size()),
            reinterpret_cast<BYTE*>(const_cast<char*>(cyphertext.data()))
        };

        auto description = LPWSTR{nullptr};
        const auto no_entropy = nullptr;
        const auto reserved_param = nullptr;
        const auto no_prompt = nullptr;
        const auto no_flags = 0;
        auto decrypted_data = DATA_BLOB{};
        CryptUnprotectData(
            &encrypted_data, &description, no_entropy, reserved_param,
            no_prompt, no_flags, &decrypted_data
        );

        const auto decrypted_bytes = decrypted_data.pbData;
        const auto decrypted_length = decrypted_data.cbData;
        const auto plaintext = QByteArray{
            reinterpret_cast<const char*>(decrypted_bytes),
            static_cast<int>(decrypted_length)
        };
        return plaintext;
    }
};

#include "Image_provider_factory.h"
#include "Image_provider.h"
#include "Safebooru_source.h"
#include "Konachan_source.h"
#include "Unsplash_source.h"
#include <QString>
#include <QObject>
#include <QDebug>
#include <exception>
#include <memory>

namespace pwall {
    std::unique_ptr<Image_provider> Image_provider_factory::get(
        const QString& source, QObject* parent
    )
    {
        qDebug() << "Get source:" << source;
        try {
            auto provider_ctor = providers.at(source);
            return provider_ctor(parent);
        } catch (const std::out_of_range&) {
            throw std::invalid_argument{
                "Source " + source.toStdString() + " is not implemented."
            };
        }
    }

    const Image_provider_factory::Map Image_provider_factory::providers{
        add<Unsplash_source>("Unsplash"),
        add<Safebooru_source>("safebooru.donmai.us"),
        add<Konachan_source>("Konachan.net"),
        // NOTE: Register your implementation here by adding
        // `add<Your_impl_class>("source name"),`.
    };

    const QString Image_provider_factory::default_provider{"Unsplash"};
};

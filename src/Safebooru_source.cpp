#include "Safebooru_source.h"
#include "Image_provider.h"
#include "Network_requester.h"
#include "Settings.h"
#include "Image_data.h"
#include <QObject>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QString>
#include <QUrlQuery>
#include <QDebug>
#include <memory>

namespace pwall {
    Safebooru_source::Safebooru_source(QObject* parent) :
        Image_provider{parent}, requester{this}
    {
        connect(
            &requester, &Network_requester::download_progress,
            this, &Safebooru_source::data_progress
        );
        connect(
            &requester, &Network_requester::succeeded,
            this, [this](std::shared_ptr<QNetworkReply> reply) {
                parse_data(*reply);
            }
        );
        connect(
            &requester, &Network_requester::failed,
            this, &Safebooru_source::failed
        );
        connect(
            &requester, &Network_requester::finished,
            this, &Safebooru_source::finished
        );
    }

    bool Safebooru_source::is_limited_by_account() const
    {
        return true;
    }

    const QUrl Safebooru_source::base_url{"https://safebooru.donmai.us"};

    void Safebooru_source::start_getting_data(const Settings& settings)
    {
        const auto post_url = safebooru_post(settings);
        requester.get(post_url);
        qDebug() << "Safebooru post URL:" << post_url;
    }

    void Safebooru_source::cancel()
    {
        requester.cancel();
    }

    void Safebooru_source::parse_data(QNetworkReply& reply)
    {
        const auto data = reply.readAll();
        const auto image_posts = QJsonDocument::fromJson(data);
        const auto post = image_posts[0];
        const auto download_url = post["file_url"].toString();
        const auto id = post["id"].toInt();
        const auto webpage_url = QString{"%1/posts/%2"}.arg(
            base_url.toString(), QString::number(id)
        );
        const auto image_data = Image_data{download_url, webpage_url};
        emit data_received(image_data);
    }

    QUrl safebooru_post(const Settings& settings)
    {
        auto post_url = QUrl{
            Safebooru_source::base_url.toString() + "/posts.json"
        };
        const auto query = safebooru_query(settings);
        post_url.setQuery(query);
        return post_url;
    }

    QUrlQuery safebooru_query(const Settings& settings)
    {
        auto query = QUrlQuery{};
        query.addQueryItem("tags", settings.tags);
        query.addQueryItem("limit", "1");
        query.addQueryItem("random", "true");
        if (!settings.username.isEmpty()) {
            query.addQueryItem("login", settings.username);
            query.addQueryItem("api_key", settings.key);
        }
        return query;
    }
};

#include "Konachan_source.h"
#include "Image_provider.h"
#include "Network_requester.h"
#include "Settings.h"
#include "Image_data.h"
#include <QObject>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QString>
#include <QUrlQuery>
#include <QDebug>
#include <random>
#include <memory>

namespace {
    // Return a random number within the interval [start, end].
    int random(int start, int end)
    {
        auto generator = std::default_random_engine{};
        auto distribution = std::uniform_int_distribution{start, end};
        return distribution(generator);
    }
}

namespace pwall {
    Konachan_source::Konachan_source(QObject* parent) :
        Image_provider{parent}, requester{this}
    {
        connect(
            &requester, &Network_requester::download_progress,
            this, &Konachan_source::data_progress
        );
        connect(
            &requester, &Network_requester::succeeded,
            this, [this](std::shared_ptr<QNetworkReply> reply) {
                parse_data(*reply);
            }
        );
        connect(
            &requester, &Network_requester::failed,
            this, &Konachan_source::failed
        );
        connect(
            &requester, &Network_requester::finished,
            this, &Konachan_source::finished
        );
    }

    bool Konachan_source::is_limited_by_account() const
    {
        return false;
    }

    const QUrl Konachan_source::base_url{"https://konachan.net"};

    void Konachan_source::start_getting_data(const Settings& settings)
    {
        const auto posts_url = konachan_posts(settings);
        requester.get(posts_url);
        qDebug() << "Konachan post URL:" << posts_url;
    }

    void Konachan_source::cancel()
    {
        requester.cancel();
    }

    void Konachan_source::parse_data(QNetworkReply& reply)
    {
        const auto data = reply.readAll();
        const auto image_posts = QJsonDocument::fromJson(data).array();
        const auto random_index = random(0, image_posts.size());
        const auto post = image_posts[random_index];
        const auto download_url = post["file_url"].toString();
        const auto id = post["id"].toInt();
        const auto webpage_url = QString{"%1/post/show/%2"}.arg(
            base_url.toString(), QString::number(id)
        );
        const auto image_data = Image_data{download_url, webpage_url};
        emit data_received(image_data);
    }

    QUrl konachan_posts(const Settings& settings)
    {
        auto posts_url = QUrl{
            Konachan_source::base_url.toString() + "/post.json"
        };
        const auto query = konachan_query(settings);
        posts_url.setQuery(query);
        return posts_url;
    }

    QUrlQuery konachan_query(const Settings& settings)
    {
        auto query = QUrlQuery{};
        const auto safe_search = " rating:s";
        query.addQueryItem("tags", settings.tags + safe_search);
        return query;
    }
};

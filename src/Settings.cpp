#include "Settings.h"
#include "Image_provider_factory.h"
#include "crypto.h"
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QDebug>
#include <algorithm>

namespace pwall {
    Settings read_settings()
    {
        const auto settings = QSettings{};
        auto my_settings = Settings{};
        my_settings.source = settings.value("source", my_settings.source)
            .toString();
        my_settings.tags = settings.value("tags", my_settings.tags).toString();
        my_settings.blur = settings.value("blur", my_settings.blur).toInt();
        my_settings.dim = settings.value("dim", my_settings.dim).toInt();
        my_settings.grey = settings.value("grey", my_settings.grey).toInt();
        my_settings.rotation_interval = settings.value(
            "rotation_interval", my_settings.rotation_interval
        ).toInt();
        my_settings.wallpaper_history_limit = settings.value(
            "wallpaper_history_limit", my_settings.wallpaper_history_limit
        ).toInt();
        my_settings.tag_history_limit = settings.value(
            "tag_history_limit", my_settings.tag_history_limit
        ).toInt();
        my_settings.username = settings.value("username", my_settings.username)
            .toString();
        my_settings.key = user_decrypt(
            settings.value("key", my_settings.key).toByteArray()
        );
        my_settings.tag_history = settings.value(
            "tag_history", my_settings.tag_history
        ).toStringList();
        my_settings.webpage_url = settings.value(
            "webpage_url", my_settings.webpage_url
        ).toString();
        my_settings.wallpaper_path = settings.value(
            "wallpaper_path", my_settings.wallpaper_path
        ).toString();
        my_settings.scheduled_time = settings.value(
            "scheduled_time", my_settings.scheduled_time
        ).toLongLong();
        return my_settings;
    }

    void write(const Settings& my_settings)
    {
        auto settings = QSettings{};
        settings.setValue("source", my_settings.source);
        settings.setValue("tags", my_settings.tags);
        settings.setValue("blur", my_settings.blur);
        settings.setValue("dim", my_settings.dim);
        settings.setValue("grey", my_settings.grey);
        settings.setValue("rotation_interval", my_settings.rotation_interval);
        settings.setValue(
            "wallpaper_history_limit", my_settings.wallpaper_history_limit
            );
        settings.setValue("tag_history_limit", my_settings.tag_history_limit);
        settings.setValue("username", my_settings.username);
        settings.setValue("key", user_encrypt(my_settings.key));
        settings.setValue("tag_history", my_settings.tag_history);
        settings.setValue("webpage_url", my_settings.webpage_url);
        settings.setValue("wallpaper_path", my_settings.wallpaper_path);
        settings.setValue("scheduled_time", my_settings.scheduled_time);
    }

    bool has_effect_prefs(const Settings& settings)
    {
        return settings.blur || settings.dim || settings.grey;
    }

    bool different_effects(const Settings& a, const Settings& b)
    {
        return a.blur != b.blur
            || a.dim != b.dim
            || a.grey != b.grey;
    }

    double brightness(const Settings& settings)
    {
        // `dim` is out of 255, so divide by 2.55 to get a percentage.
        const auto percent_dim = settings.dim / 2.55;
        // Dimness is the opposite of brightness, so subtract from 100
        // to convert from dimness to brightness percentage.
        return 100 - percent_dim;
    }

    double saturation(const Settings& settings)
    {
        // `grey` is out of 500, so divide by 5.0 to get a percentage.
        const auto percent_desaturated = settings.grey / 5.0;
        // Desaturation is the opposite of saturation, so subtract from
        // 100 to convert from desaturation to saturation percentage.
        return 100 - percent_desaturated;
    }

    QStringList trim_tag_history(const Settings& settings)
    {
        const auto tag_history = settings.tag_history;
        const auto length = tag_history.length();
        const auto diff = std::max(length - settings.tag_history_limit, 0);
        return tag_history.mid(diff);
    }
};

#include "image_provider_tests.h"
#include "Image_provider.h"
#include "Image_provider_factory.h"
#include "Safebooru_source.h"
#include "Konachan_source.h"
#include "Unsplash_source.h"
#include <QtTest>
#include <memory>
#include <exception>

void Test_image_provider_factory::get()
{
    auto provider = pwall::Image_provider_factory::get("safebooru.donmai.us");
    QVERIFY(
        is_ptr_to_derived<pwall::Safebooru_source>(provider.get())
    );

    provider = pwall::Image_provider_factory::get("Konachan.net");
    QVERIFY(
        is_ptr_to_derived<pwall::Konachan_source>(provider.get())
    );

    provider = pwall::Image_provider_factory::get("Unsplash");
    QVERIFY(
        is_ptr_to_derived<pwall::Unsplash_source>(provider.get())
    );

    QVERIFY_EXCEPTION_THROWN(
        pwall::Image_provider_factory::get("FAIL"),
        std::invalid_argument
    );
}

QTEST_MAIN(Test_image_provider_factory)

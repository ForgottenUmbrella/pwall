#ifndef IMAGE_PROVIDER_TESTS_H
#define IMAGE_PROVIDER_TESTS_H

#include <QObject>

// Test the `image_provider_factory` class' interface.
class Test_image_provider_factory : public QObject
{
    Q_OBJECT
private slots:
    // Ensure `get` returns the right implementation of `image_provider`.
    void get();
};

// Return whether a pointer to a base class is actually to a derived class.
template<class derived, class base>
bool is_ptr_to_derived(base* ptr)
{
    return dynamic_cast<derived*>(ptr);
}

#endif

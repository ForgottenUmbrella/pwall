#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "ui_Main_window.h"
#include "Image_provider_factory.h"
#include "Settings.h"
#include "Notifier.h"
#include <QMainWindow>

class QWidget;
class QPushButton;
class QPixmap;
class QUrl;
class QString;

namespace pwall {

    // The main window of this application.
    class Main_window : public QMainWindow {
        Q_OBJECT
    public:
        explicit Main_window(QWidget* parent = nullptr);
    private:
        Ui::Main_window ui;
        Notifier notifier;
        // Return a reference to the "Apply" button in the button box.
        QPushButton& get_apply_button();
        // Fill the list of sources with the registered implementations.
        void populate_source_input(const Image_provider_factory::Map& providers);
        // Synchronise the edit sliders and spinboxes.
        void sync_sliders();
        // Enable "Apply" button when settings change.
        void enable_apply_button_on_change();
    private slots:
        // Load sign-in warning image with aspect ratio maintained.
        void load_warning_image(const QPixmap& warning_image);
        // Load settings into UI.
        void load_into_ui(const Settings& settings);
        // Enable or disable the "Apply" button.
        void enable_apply_button(bool enabled = true);
        // Enable or disable the "Manage Account" button and warning.
        void enable_sign_in(bool enabled = true);
        // Save changes to the settings file and return it.
        Settings save_settings() const;
        // Set the "View wallpaper online" label URL.
        void set_link_url(const QUrl& url);
        // Get the next wallpaper, and show a progress window.
        void next_wallpaper(const Settings& settings);
        // Edit the wallpaper after saving settings.
        // This will show a progress window.
        void edit_wallpaper(const Settings& settings);
        // Edit the wallpaper if effect preferences have changed.
        void edit_if_changed(
            const Settings& old_settings, const Settings& new_settings
        );
        // Change the GUI to accomodate wallpaper source peculiarities.
        void update_ui_for_source(const QString& source);
    };

    // Open the cache directory containing previous wallpapers.
    void open_image_dir();
    // Force-show a widget's tooltip.
    void show_tooltip(const QWidget& widget);
    // Open a dialog window, wait for it to close, and return its exit code.
    template<class Dialog>
    int open_modal()
    {
        auto dialog = Dialog{};
        return dialog.exec();
    }
};

#endif

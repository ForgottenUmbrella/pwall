#ifndef IMMEDIATE_TOOLTIP_H
#define IMMEDIATE_TOOLTIP_H

#include <QEvent>
#include <QToolTip>
#include <QPoint>
#include <QLabel>

namespace pwall {
    // Mixin to make a QWidget's tooltip appear immediately on hover.
    // To use, publicly inherit a specialisation of this template (since
    // you'll need the `Q_OBJECT` macro) on your desired QWidget, include
    // the `Q_OBJECT` macro, and inherit the base class' constructor.
    // See `Immediate_label` for an example.
    template<class Widget>
    class Immediate_tooltip : public Widget {
    public:
        using Widget::Widget;
    protected:
        void enterEvent(QEvent*) final
        {
            Widget::setMouseTracking(true);
            const auto position = Widget::mapToGlobal(QPoint{0, 0});
            QToolTip::showText(position, Widget::toolTip(), this);
        }
    };

    // Label that shows tooltip immediately on hover.
    class Immediate_label : public Immediate_tooltip<QLabel> {
        Q_OBJECT
    private:
        using Super = Immediate_tooltip<QLabel>;
    public:
        using Super::Super;
    };
};

#endif

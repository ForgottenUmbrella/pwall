#ifndef SETTINGS_H
#define SETTINGS_H

#include "Image_provider_factory.h"
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QUrl>

class QSettings;

namespace pwall {
    // Settings for this program.
    struct Settings {
        // The online source for wallpapers.
        QString source = Image_provider_factory::default_provider;
        // Space-delimited tags for searching.
        QString tags = "";
        // The blur radius (in pixels) for editing.
        // Range is 0 to 500.
        int blur = 0;
        // Dim amount to compute relative brightness percentage.
        // Range is 0 to 255.
        int dim = 0;
        // Grey amount to compute relative saturation percentage.
        // Range is 0 to 500.
        int grey = 0;
        // The number of hours to wait before changing the wallpaper.
        int rotation_interval = 24;
        // The number of wallpapers to cache.
        int wallpaper_history_limit = 10;
        // The number of previous tag sets to store.
        int tag_history_limit = 10;
        // The username for source authentication.
        QString username = "";
        // The key (e.g. password or API key) for source authentication.
        QByteArray key = "";
        // The list of previous tag sets used.
        QStringList tag_history = {};
        // The URL of the current wallpaper.
        QUrl webpage_url = {};
        // The path of the current wallpaper.
        QString wallpaper_path = "";
        // The next UNIX time (in seconds) to change the wallpaper.
        long long scheduled_time;
    };

    // Return a Settings object with settings read from disk.
    Settings read_settings();
    // Save settings to disk.
    void write(const Settings& my_settings);
    // Return whether the user has settings for editing the wallpaper.
    bool has_effect_prefs(const Settings& settings);
    // Return whether there effect preferences are different.
    bool different_effects(const Settings& a, const Settings& b);
    // Return the desired brightness (dimness) as a relative percentage.
    double brightness(const Settings& settings);
    // Return the desired (de)saturation as a relative percentage.
    double saturation(const Settings& settings);
    // Return shortened tag history with `tag_history_limit` items max.
    QStringList trim_tag_history(const Settings& settings);
};

#endif

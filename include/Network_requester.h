#ifndef NETWORK_REQUESTER_H
#define NETWORK_REQUESTER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <memory>

class QUrl;

namespace pwall {
    // Class for making network requests without manual management of
    // `QNetworkReply`.
    // To use, call the `get` method, and connect to the `succeeded` signal.
    class Network_requester : public QObject {
        Q_OBJECT
    public:
        explicit Network_requester(QObject* parent = nullptr);
    public slots:
        // Make an asynchronous GET request.
        void get(const QUrl& url);
        // Abort the request.
        void cancel();
    signals:
        // Progress receiving data in bytes, from `reply_manager`.
        void download_progress(long long received, long long total);
        // Download or upload has finished successfully.
        void succeeded(std::shared_ptr<QNetworkReply> reply);
        // Download or upload has thrown an error.
        void failed(QString reason);
        // Download or upload has either succeeded or failed.
        void finished();
    private slots:
        // Selectively emit either `succeeded` or `failed`.
        void success_or_error(QNetworkReply& reply);
    private:
        QNetworkAccessManager network_manager;
        std::shared_ptr<QNetworkReply> reply_manager;
    };

    // Convert a network error code to a more meaningful message.
    QString error_message(QNetworkReply::NetworkError error_code);
};

#endif

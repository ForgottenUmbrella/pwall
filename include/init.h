#ifndef INIT_H
#define INIT_H

#include <QApplication>
#include <memory>

namespace pwall {
    // Initialise settings and return a pointer to the application.
    std::unique_ptr<QApplication> init(int argc, char* argv[]);
};

#endif

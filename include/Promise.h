#ifndef PROMISE_H
#define PROMISE_H

#include <QObject>
#include <QEventLoop>
#include <utility>
#include <functional>

namespace pwall {
    // An awaitable wrapper over Qt APIs to start asynchronous
    // operations and synchronously wait for a signal later.
    //
    // Example:
    // auto network_manager = QNetworkAccessManager{};
    // auto promise = Promise{
    //   [&network_manager]{
    //     network_manager.get("www.example.com");
    //   }, network_manager, &QNetworkAccessManager::finished
    // };
    // QNetworkReply* reply;
    // QObject::connect(
    //     &network_manager, &QNetworkAccessManager::finished,
    //     [reply](QNetworkReply* reply_) {
    //         reply = reply_;
    //     }
    // );
    // // Some time afterwards...
    // promise.await();
    template<class Object, typename... Args>
    class Promise {
        using Signal = void(Object::*)(Args...);
    public:
        Promise(
            std::function<void()> async_operation,
            const Object& object, Signal signal
        )
        {
            QObject::connect(
                &object, signal,
                &waiter, &QEventLoop::quit,
                Qt::QueuedConnection
            );
            async_operation();
        }
        // Block until signal is emitted.
        void await()
        {
            waiter.exec();
        }
    private:
        QEventLoop waiter{};
    };
};

#endif

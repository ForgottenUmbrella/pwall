#ifndef WALLPAPER_CHANGER_H
#define WALLPAPER_CHANGER_H

#include "Downloader.h"
#include "Image_provider.h"
#include "Image_data.h"
#include <QObject>
#include <QDir>
#include <QString>
#include <QUrl>
#include <QFutureWatcher>
#include <memory>

namespace pwall {
    struct Settings;
    struct Image_data;

    // Signals/slots-based class for changing the wallpaper periodically.
    // To use, call the `start` method.
    class Wallpaper_changer : public QObject {
        Q_OBJECT
    public:
        explicit Wallpaper_changer(QObject* parent = nullptr);
    public slots:
        // Set the wallpaper to the next (edited) image,
        // clear old images and set the schedule, asynchronously.
        void start(const Settings& settings);
        // Cancel getting image data and downloading the image.
        void cancel();
        // Edit and set the wallpaper if effect preferences set.
        void maybe_edit(const Settings& settings);
    signals:
        // Image data progress, forwarded from `provider`.
        void data_progress(long long received, long long total);
        // Image data has arrived, forwarded from `provider`.
        void data_received(Image_data data);
        // Image download progress, forwarded from `image_downloader`.
        void download_progress(long long received, long long total);
        // The image has been downloaded, from `image_downloader`.
        void downloaded();
        // The wallpaper is ready to be set, e.g. after maybe editing.
        void wallpaper_ready(QString wallpaper_path);
        // The wallpaper has been successfully changed.
        void succeeded();
        // Getting image data or downloading has failed.
        void failed(QString reason);
        // Wallpaper-changing process has finished (perhaps failed).
        void finished();
        // The webpage URL has changed.
        void webpage_url_changed(QUrl url);
    private slots:
        // Download an image asynchronously.
        void start_downloading(const Image_data& data);
        // Save the webpage URL from the new wallpaper.
        void save_webpage_url(const Image_data& data);
    private:
        std::unique_ptr<Image_provider> provider;
        Downloader image_downloader;
        QFutureWatcher<QString> edits_watcher;
    };

    // Save the wallpaper path so it can be re-edited.
    void save_wallpaper_path(const QString& path);
    // Reset the scheduled time for the next wallpaper change.
    void schedule();
    // Delete old images to not use excessive storage space.
    void trim_cache(const Settings& settings);
    // Return path of the cache directory where images are stored.
    // It's a function instead of a variable since it needs delayed
    // evaluation so the application name can be set in `main`
    // beforehand.
    QDir image_dir();
    // Return the path of the subdirectory where edited images are.
    QDir edits_dir();
    // Return the path of the edited image.
    QString edited_path(const QString& path);
    // Create nested directories if they don't already exist.
    void mkdir_nested(const QDir& path);
};

#endif

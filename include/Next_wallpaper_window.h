#ifndef NEXT_WALLPAPER_WINDOW_H
#define NEXT_WALLPAPER_WINDOW_H

#include "ui_Next_wallpaper_window.h"
#include "Wallpaper_changer.h"
#include <QDialog>

class QString;

namespace pwall {
    // Modal dialog for showing download progress of the new wallpaper.
    class Next_wallpaper_window : public QDialog {
        Q_OBJECT
        // Provide `Main_window` with access to the `changer`.
        friend class Main_window;
    public:
        explicit Next_wallpaper_window(QWidget* parent = nullptr);
    private slots:
        // Set progress bar maximum and update current value.
        void set_progress(int received, int total);
        // Set progress bar to a "busy" state.
        void set_unknown_progress();
        // Set the label text to state that it's currently downloading.
        void set_downloading_message();
        // Set the label text to state that it's currently editing.
        void set_editing_message();
        // Disable Cancel button so editing thread can't be aborted.
        // Otherwise it would crash.
        void disable_cancel();
    private:
        Ui::Next_wallpaper_window ui;
        Wallpaper_changer changer;
    };
};

#endif

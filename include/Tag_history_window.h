#ifndef TAG_HISTORY_WINDOW_H
#define TAG_HISTORY_WINDOW_H

#include "ui_Tag_history_window.h"
#include <QDialog>

class QWidget;

namespace pwall {
    struct Settings;

    // Modal dialog for managing tag history.
    class Tag_history_window : public QDialog {
        Q_OBJECT
    public:
        explicit Tag_history_window(QWidget* parent = nullptr);
    private:
        Ui::Tag_history_window ui;
    private slots:
        // Load tag history into UI.
        void load_history(const Settings& settings);
        // Save edited tag history.
        void save_history() const;
        // Disable the "Remove" button if the tag history list is empty.
        void disable_remove_if_empty();
    };
};

#endif

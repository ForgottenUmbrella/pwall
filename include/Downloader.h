#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include "Network_requester.h"
#include <QObject>
#include <QString>

class QUrl;

namespace pwall {
    // Signals/slots-based class for downloading a file asynchronously.
    // To use, call the `start` method and connect to the `succeeded`
    // signal to know when it's done.
    class Downloader : public QObject {
        Q_OBJECT
    public:
        explicit Downloader(QObject* parent = nullptr);
    public slots:
        // Start downloading the URL to a path.
        // On subsequent calls, any pending downloads are cancelled.
        void start(const QUrl& url, const QString& path);
        // Cancel downloading the file.
        void cancel();
    signals:
        // Download progress in terms of bytes.
        void progress(long long received, long long total);
        // The download has successfully finished.
        void succeeded(QString path);
        // The download has failed.
        void failed(QString reason);
        // The download has succeeded or failed.
        void finished();
    private:
        Network_requester requester;
        // Store the path passed to `get` for the
        // `Network_requester::succeeded` connection to save to.
        QString write_path;
    };

    // Sequentially read reply data to file, emptying the reply.
    void save(QIODevice& reply, const QString& path);
};

#endif

#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <QObject>
#include <QSystemTrayIcon>
#include <QString>
#include <QNetworkReply>

namespace pwall {
    class Wallpaper_changer;

    // Signals/slots-based class for notifying PWall errors.
    class Notifier : public QObject {
        Q_OBJECT
    public:
        explicit Notifier(QObject* parent = nullptr);
        // Send a generic notification.
        void notify(
            const QString& title, const QString& message,
            QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::NoIcon,
            int timeout = 10000
        );
    public slots:
        // Make the system tray icon visible.
        void show();
        // Send an error notification.
        void error(const QString& message);
    private:
        QSystemTrayIcon system_tray_icon;
    };
};

#endif

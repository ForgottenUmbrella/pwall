#ifndef IMAGE_PROVIDER_FACTORY_H
#define IMAGE_PROVIDER_FACTORY_H

#include <QString>
#include <map>
#include <utility>
#include <memory>

class QObject;

namespace pwall {
    class Image_provider;

    // Static factory for instances of the `Image_provider` interface.
    class Image_provider_factory {
    public:
        // Return an appropriate `Image_provider` implementation.
        // Throw a `std::invalid_argument` error if none exists
        // with the given `source` name.
        static std::unique_ptr<Image_provider> get(
            const QString& source, QObject* parent = nullptr
        );
        // Type alias for factory functions.
        using Provider_creator = std::unique_ptr<Image_provider> (*)(QObject*);
        // Type alias for the `providers` map template specialisation.
        using Map = std::map<QString, Provider_creator>;
        // Collection of registered implementation names and factories.
        static const Map providers;
        // Default provider for the GUI.
        static const QString default_provider;
    private:
        // Factory function for registering implementations,
        // since pointers to ctors are forbidden.
        template<class Provider>
        static std::unique_ptr<Image_provider> create(QObject* parent = nullptr)
        {
            return std::make_unique<Provider>(parent);
        }
        // Return a pair to store in the `providers` map,
        // in order to add an `Image_provider` implementation.
        template<class Provider>
        static std::pair<QString, Provider_creator> add(const QString& name)
        {
            return {name, &create<Provider>};
        }
    };

};

#endif

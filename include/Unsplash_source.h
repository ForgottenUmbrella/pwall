#ifndef UNSPLASH_SOURCE_H
#define UNSPLASH_SOURCE_H

#include "Image_provider.h"
#include "Network_requester.h"
#include <QUrl>
#include <QUrlQuery>

class QObject;
class QNetworkReply;

namespace pwall {
    struct Settings;

    // Implementation of `Image_provider` for Unsplash.
    class Unsplash_source : public Image_provider {
        Q_OBJECT
    public:
        Unsplash_source(QObject* parent = nullptr);
        // Return `false`: Unsplash works the same regardless of account.
        bool is_limited_by_account() const override;
    public slots:
        void start_getting_data(const Settings& settings) override;
        void cancel() override;
    private slots:
        // Parse network reply into an `Image_data` object and emit it.
        void parse_data(QNetworkReply& reply);
    private:
        Network_requester requester;
    };

    // Return a URL to a random image post on Unsplash.
    QUrl unsplash_post(const Settings& settings);
    // Return the URL query for `get_random_post`.
    QUrlQuery unsplash_query(const Settings& settings);
};

#endif

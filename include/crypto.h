#ifndef CRYPTO_H
#define CRYPTO_H

#include <QByteArray>

namespace pwall {
    // Encrypt a string using the current user's credentials as the key.
    // This allows for automatic encryption/decryption of the source API
    // key/password, meaning it is stored securely.
    QByteArray user_encrypt(const QByteArray& plaintext);
    // Decrypt a string using the current user's credentials as the key.
    QByteArray user_decrypt(const QByteArray& cyphertext);
}

#endif

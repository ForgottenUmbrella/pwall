#ifndef KONACHAN_SOURCE_H
#define KONACHAN_SOURCE_H

#include "Image_provider.h"
#include "Network_requester.h"
#include <QUrl>
#include <QUrlQuery>

class QObject;
class QNetworkReply;

namespace pwall {
    struct Settings;

    // Implementation of `Image_provider` for Konachan.net.
    class Konachan_source : public Image_provider {
        Q_OBJECT
    public:
        Konachan_source(QObject* parent = nullptr);
        // Return `false`: Konachan works the same regardless of account.
        bool is_limited_by_account() const override;
        // Konachan.net's home page.
        static const QUrl base_url;
    public slots:
        void start_getting_data(const Settings& settings) override;
        void cancel() override;
    private slots:
        // Parse network reply into an `Image_data` object and emit it.
        void parse_data(QNetworkReply& reply);
    private:
        Network_requester requester;
    };

    // Return a URL to a number of image posts on Konachan.
    QUrl konachan_posts(const Settings& settings);
    // Return the URL query for `get_random_post`.
    QUrlQuery konachan_query(const Settings& settings);
};

#endif

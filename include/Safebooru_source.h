#ifndef SAFEBOORU_SOURCE_H
#define SAFEBOORU_SOURCE_H

#include "Image_provider.h"
#include "Network_requester.h"
#include <QUrl>
#include <QUrlQuery>

class QObject;
class QNetworkReply;

namespace pwall {
    struct Settings;

    // Implementation of `Image_provider` for safebooru.donmai.us.
    class Safebooru_source : public Image_provider {
        Q_OBJECT
    public:
        Safebooru_source(QObject* parent = nullptr);
        // Return `true`: available tags are limited by account level.
        bool is_limited_by_account() const override;
        // Safebooru's home page.
        static const QUrl base_url;
    public slots:
        void start_getting_data(const Settings& settings) override;
        void cancel() override;
    private slots:
        // Parse network reply into an `Image_data` object and emit it.
        void parse_data(QNetworkReply& reply);
    private:
        Network_requester requester;
    };

    // Return a URL to a random image post on safebooru.
    QUrl safebooru_post(const Settings& settings);
    // Return the URL query for `get_random_post`.
    QUrlQuery safebooru_query(const Settings& settings);
};

#endif

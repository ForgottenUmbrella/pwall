#ifndef EFFECTS_H
#define EFFECTS_H

#include <Magick++.h>

class QString;

namespace pwall {
    struct Settings;

    // Blur, dim and desaturate an image, then save it.
    void edit(
        const QString& source, const QString& destination,
        const Settings& settings
    );

    // Return a blurred image.
    // The purpose of scaling down is two-fold: it exaggerates the
    // effects of the blur, and also speeds up processing.
    // Based on Muzei's `MuzeiBlurRenderer.constructor.load` function.
    Magick::Image scaled_blur(Magick::Image image, double radius);
    // Calculate and return the blur radius, based on the screen size.
    // Based on Muzei's
    // `MuzeiBlurRenderer.recomputeMaxPrescaledBlurPixels` function.
    double blur_radius(const Settings& settings);
    // Return an image scaled to a given ratio.
    Magick::Image scale(Magick::Image image, double ratio);
};

#endif

#ifndef IMAGE_DATA_H
#define IMAGE_DATA_H

#include <QUrl>

namespace pwall {
    // Data necessary for this program to download and view images.
    struct Image_data {
        QUrl download_url;
        QUrl webpage_url;
    };
};

#endif

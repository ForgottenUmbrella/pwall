#ifndef WALLPAPER_H
#define WALLPAPER_H

class QString;

namespace pwall {
    // Set the destkop wallpaper to an image file at a given `path`.
    void set_wallpaper(const QString& path);
};

#endif

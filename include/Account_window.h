#ifndef ACCOUNT_WINDOW_H
#define ACCOUNT_WINDOW_H

#include "ui_Account_window.h"
#include <QDialog>

class QWidget;
class QString;

namespace pwall {
    struct Settings;

    // Modal dialog for managing accounts for sources that require them.
    class Account_window : public QDialog {
        Q_OBJECT
    public:
        explicit Account_window(QWidget* parent = nullptr);
    public slots:
        // Close window only if credentials are valid.
        void accept() override;
    private:
        Ui::Account_window ui;
    private slots:
        // Load stored credentials into UI.
        void load_into_ui(const Settings& settings);
        // Save credentials from UI.
        void save_credentials() const;
        // Enable the remove button if the username or key is non-empty.
        // Otherwise, disable it.
        void enable_remove_button(const QString& new_text);
    };

    // Return whether the combination of username and key is valid.
    bool is_valid(const QString& username, const QString& key);
};

#endif

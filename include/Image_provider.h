#ifndef IMAGE_PROVIDER_H
#define IMAGE_PROVIDER_H

#include "Image_data.h"
#include <QObject>
#include <QString>

namespace pwall {
    struct Settings;

    // Interface for a signals/slots-based online image provider.
    class Image_provider : public QObject {
        Q_OBJECT
    public:
        using QObject::QObject;
        virtual ~Image_provider() noexcept;
        // Return whether the UI needs to accomodate signing in.
        virtual bool is_limited_by_account() const = 0;
    public slots:
        // Get image data asynchronously.
        virtual void start_getting_data(const Settings& settings) = 0;
        // Cancel getting image data.
        virtual void cancel() = 0;
    signals:
        // Progress made getting image data, in bytes.
        void data_progress(long long received, long long total);
        // Image data has arrived.
        void data_received(Image_data image_data);
        // Getting image data has failed.
        void failed(QString reason);
        // Image data has either arrived or failed.
        void finished();
    };
};

#endif
